import { EVENT_TYPE, gameRatio } from "../constants/eventType";
import axios from "axios";
import {
  IMouseEvent,
  IGameSessionEvent,
  IGameSessionEventsCreatePayload,
  IGameSessionEventsUpdatePayload,
  IRegisterEventsConstructor,
} from "../types/dataInterface";

export class RegisterEvents {
  static ROOT_URL = "http://localhost:4100";
  gameSessionEvents: IGameSessionEvent[] = [];
  startTime: number = 0;

  constructor({ startTime }: IRegisterEventsConstructor) {
    this.initialize(startTime);
  }

  setDefault = () => {
    console.log("[Register events] - set default");
    this.gameSessionEvents = [{ type: EVENT_TYPE.PING }];
  };

  initialize = (startTime: number) => {
    this.setDefault();
    this.startTime = startTime;
    this.gameSessionEvents = [{ type: EVENT_TYPE.PING }];
  };

  /**
   * Register all event
   * @returns
   */
  register = () => {
    const htmlGameCanvas = document.getElementById("UT_CANVAS");
    let isMouseDown = false;

    const mousedownCallback = (event: MouseEvent) => {
      isMouseDown = true;
      this.saveMouseEvent(event);
    };
    const mouseupCallback = (event: MouseEvent) => {
      this.saveMouseEvent(event);
      isMouseDown = false;
    };
    const mousemoveCallback = (event: MouseEvent) => {
      isMouseDown && this.saveMouseEvent(event);
    };
    const touchstartCallback = (event: TouchEvent) => {
      this.saveTouchEvent(event);
      isMouseDown = true;
    };
    const touchmoveCallback = (event: TouchEvent) => {
      isMouseDown && this.saveTouchEvent(event);
    };
    const touchendCallback = (event: TouchEvent) => {
      this.saveTouchEvent(event);
      isMouseDown = false;
    };

    htmlGameCanvas?.addEventListener("mousedown", mousedownCallback);
    htmlGameCanvas?.addEventListener("mouseup", mouseupCallback);
    htmlGameCanvas?.addEventListener("mousemove", mousemoveCallback);
    htmlGameCanvas?.addEventListener("touchstart", touchstartCallback);
    htmlGameCanvas?.addEventListener("touchmove", touchmoveCallback);
    htmlGameCanvas?.addEventListener("touchend", touchendCallback);

    return (): void => {
      htmlGameCanvas?.removeEventListener("mousedown", mousedownCallback);
      htmlGameCanvas?.removeEventListener("mouseup", mouseupCallback);
      htmlGameCanvas?.removeEventListener("mousemove", mousemoveCallback);
      htmlGameCanvas?.removeEventListener("touchstart", touchstartCallback);
      htmlGameCanvas?.removeEventListener("touchmove", touchmoveCallback);
      htmlGameCanvas?.removeEventListener("touchend", touchendCallback);
    };
  };

  /**
   * Set prescription follow screen push MouseEvent to gameSessionEvents
   * @param event Mouse event from eventListener
   */
  saveMouseEvent = (event: IMouseEvent) => {
    let screenX = window.innerWidth,
      screenY = window.innerHeight;
    const canvasRatio = screenX / screenY;

    if (canvasRatio > gameRatio) {
      screenX = window.innerHeight * gameRatio;
    } else screenY = window.innerWidth / gameRatio;

    const canvasEvent = {
      type: event.type,
      screenX: screenX,
      screenY: screenY,
      clientX: event.clientX - (window.innerWidth - screenX) / 2,
      clientY: event.clientY - (window.innerHeight - screenY) / 2,
    };

    const time = Date.now() - this.startTime;

    this.gameSessionEvents.push({
      type: EVENT_TYPE.INPUT,
      time: time,
      data: JSON.stringify(canvasEvent),
    });
  };

  /**
   * Save touch Event
   * @param event - Touch event from eventListener
   */
  saveTouchEvent = (event: TouchEvent) => {
    const touch = event.changedTouches[0];
    const touchEvent = {
      type: event.type,
      clientX: touch.clientX,
      clientY: touch.clientY,
      screenX: touch.screenX,
      screenY: touch.screenY,
    };

    this.saveMouseEvent(touchEvent as IMouseEvent);
  };

  /**
   *
   * @param {Object} payload - Object GameSessionId + first event
   * @returns
   */
  static async createGameSessionEvents(
    payload: IGameSessionEventsCreatePayload
  ): Promise<any> {
    try {
      const fullURL = `${this.ROOT_URL}/create`;
      return axios.post(fullURL, payload);
    } catch (e) {
      console.log(e);
    }
  }

  /**
   *
   * @param {Object} payload - Object GameSessionId + list event to update
   * @returns
   */
  static async updateGameSessionEvents(
    payload: IGameSessionEventsUpdatePayload
  ): Promise<any> {
    try {
      const fullURL = `${this.ROOT_URL}/update/`;
      return axios.put(fullURL, payload);
    } catch (e) {
      console.log(e);
    }
  }
}
